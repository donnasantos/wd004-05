// // Task: To get the sum of all the number from 1-10
// let number = 0;
// let sum = 0;

// while (number < 11){
// 	sum += number;

// 	// a way to terminate the condition
// 	number ++;
// } 

// console.log(sum);

// do-while

// do it once

let age = 9;

do{
	// task you want to accomplish
	console.log("Your age is:" + age);
	// terminating process
	age ++;
}while(age < 5);
