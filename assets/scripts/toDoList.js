
const todos = [];

let taskId = 1;

function addToDo(task){
	const newToDo = {
		task: task,
		id: taskId,
		isDone: false
	}

	todos.push(newToDo);

// once we're done adding the newtodo, we must increment the taskID
	taskId++;

	return "Successfully added" + task;
}

function showAllTodos(){
	for( let index = 0; index < todos.length; index++){
		console.log("Task ID: " + todos[index].id + ", Task Name: " + todos[index].task + ", Is Done?: " + todos[index].isDone.toString());

	}
}

function markAsDone(taskId){
	// I need to find the task with the corresponding taskID. i eed to loop through my current to dos array
	for( let index = 0; index < todos.length; index++){
		if(taskId === todos[index].id){
			todos[index].isDone = true;
		}
	}

	return "Success";
}