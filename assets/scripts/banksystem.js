// Bank System
// 1. addCustomer(name)
	// customer ={
	// 	name: name
	// 	bankAccount Number: bankAccountNumber,
	// 	balance: 0
	// 	isActive: true

	// }

	// 2.showAllCustomers(){
	// 	//must show all customers individually
	// }

	// 3. freezeCustomer(bankAcctNumber){
	// 	must change the isActive status to false
	// }

	// 4.unFreezeCustomer(bankAcctNumber){
	// 	must change the isActive status to True
	// }
	// 5. deposit(bankAccountNumber, amount){
	// 	must add the amount to the balance
	// }

	// 6, withdraw(bankAccountNumber, amount){
	// 	must deduct the amount
	// }
	// 7. showAllFrozenCustomer(){
	//will only show customers with isActive === false;
//}
// 8. showAllActiveCustomer(){
// 	//will only show customers with isActive === true;
// }
// To get an S:
// 9. Make sure to validate data. 
// 10. Meaning, you can't withdraw more than your acct. balance
// 11. You can't deposit a negative amount.


const customer = [];

let bankAccountNumber = 1;

function addCustomer(client){
	const newCustomer = {
		name: client,
		accountNumber: bankAccountNumber,
		balance: 0,
		isActive: true,
	}
		customer.push(newCustomer);
		bankAccountNumber++;
		return "Successfully Added: " + client;
}

function showAllCustomers(){
	for( let index=0; index < customer.length; index++){
		console.log("Name: " + customer[index].name + ", Bank Account Number: " + customer[index].accountNumber + ", Balance: " + customer[index].balance + ", isActive: " + customer[index].isActive.toString());
	}
}


function freezeCustomer(bankAcctNumber){
	for ( let index=0; index < customer.length; index++){
		if( bankAcctNumber === customer[index].accountNumber && customer[index].isActive === true){
			customer[index].isActive = false;
			return "Temporarily Inactive. Account is freeze. Account Number: " + bankAcctNumber;
		}
	}
	return "The account is already considered frozen account.";
}


function unFreezeCustomer(bankAcctNumber){
	for ( let index=0; index < customer.length; index++){
		if( bankAcctNumber === customer[index].accountNumber && customer[index].isActive === false){
			customer[index].isActive = true;
			return "Activated Account Number: " + bankAcctNumber;
		}
	}	
	return "The account is already activated.";
}


function deposit(bankAcctNumber, amount){
	for ( let index=0; index < customer.length; index++){
		if( bankAcctNumber === customer[index].accountNumber && amount > 0 && customer[index].isActive === true){
			customer[index].balance += amount;
			return "Succefully deposited to " + customer[index].accountNumber + " an amount of " + amount; 
		}
	}
	return "Invalid. Check the amount and status."
}

function withdraw(bankAcctNumber, amount){
	for ( let index=0; index < customer.length; index++){
		if( bankAcctNumber === customer[index].accountNumber && customer[index].balance >= amount && amount > 0 && customer[index].isActive === true){
			customer[index].balance -= amount;
			return "Succefully Withdrawn from Bank Account Number " + customer[index].accountNumber + " an amount of " + amount; 
		}
	}
	return "Invalid. Check the amount and status."
}


function showAllFrozenCustomer(){
	for( let index=0; index < customer.length; index++){
		if( customer[index].isActive === false){
			console.log("Name: " + customer[index].name + ", Bank Account Number: " + customer[index].accountNumber + ", Balance: " + customer[index].balance + ", isActive: " + customer[index].isActive.toString());
			}
		}
	}

function showAllActiveCustomer(){
	for( let index=0; index < customer.length; index++){
		if( customer[index].isActive === true){
			console.log("Name: " + customer[index].name + ", Bank Account Number: " + customer[index].accountNumber + ", Balance: " + customer[index].balance + ", isActive: " + customer[index].isActive.toString());
			}
		}
	}